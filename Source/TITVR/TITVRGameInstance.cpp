// Fill out your copyright notice in the Description page of Project Settings.

#include "TITVR.h"
#include "TITVRGameInstance.h"




UTITVRGameInstance::UTITVRGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//Platform = GetModuleHandle(TEXT("Platform.dll"));
	//if(!Platform)
	Platform = LoadLibrary(TEXT("Platform.dll"));
	
	Oculus = LoadLibrary(TEXT("OculusPlugin.dll"));

	

	if (GetProcAddress(Oculus, "OVR_GetSensorOrientation"))
	{
		error += 1;
	}

	//OculusPlugin
	(FARPROC &)OVR_GetSensorOrientation = GetProcAddress(Oculus, "OVR_GetSensorOrientation");
	(FARPROC &)OVR_Initialize = GetProcAddress(Oculus, "OVR_Initialize");

	//TITPlatform
	(FARPROC &)TIConnect = GetProcAddress(Platform, "TIConnect" );

	(FARPROC &)TIConnectInt = GetProcAddress(Platform, "TIConnectInt");

	(FARPROC &)TISendF = GetProcAddress(Platform, "TISendF");

	(FARPROC &)TISendI = GetProcAddress(Platform, "TISendI");

	(FARPROC &)TIDisconnect = GetProcAddress(Platform, "TIDisconnect");

	(FARPROC &)TIGetLastError = GetProcAddress(Platform, "TIGetLastError");

	(FARPROC &)TIRead = GetProcAddress(Platform, "TIRead");

	//TINormalModeCallback callback = 0;
	//a = TIConnectInt(3, TI_MODE_NORMAL, 0);
	//END();
	//START();
}

void UTITVRGameInstance::Init()
{
	//InitializeStandalone();
	Super::Init();
}

void UTITVRGameInstance::StartGameInstance()
{
	
}

FQuat UTITVRGameInstance::GetOrientation(int id)
{
	float w, x, y, z;
	bool b = OVR_GetSensorOrientation(id, &w, &x, &y, &z);
	//FQuat q = FQuat(z, -x, -y, w);		   
	return FQuat(z, -x, -y, w);
	
}


/*void UTITVRGameInstance::GetPlatformOrientation(int16 * PlatformPitch, int16 * PlatformYaw)
{
	TIRead(PlatformYaw, PlatformPitch, nullptr, nullptr, nullptr, nullptr, nullptr);
}

void UTITVRGameInstance::GetJoiystickOrientation(int16 * JPitch, int16 * JYaw)
{
	TIRead(nullptr, nullptr, JYaw, JPitch, nullptr, nullptr, nullptr);
	//return FRotator((float)JPitch, (float)JYaw, 0);
}*/

void UTITVRGameInstance::TEST()
{
	char a, b; 
	short c;

	/*if (GetProcAddress(Platform, "TIRead"))
	{
		errorP = 100;
	}*/
	//errorP = TISendI(axis4, axis3, 255, 0);

	GEngine->AddOnScreenDebugMessage(-1, 0.0001, FColor::Green, FString(TEXT("TEST")));

	if(TISendF(axis3 / 300.f, axis4 /= 300.f, 0.2, 0))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("SEND")));
	if(TIRead(&axis1, &axis2, &axis3, &axis4, &a, &b, &c))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));
	
	
	//axis3 /= (32000.f;
	//axis4 /= 32000.f;
}

/*UTITVRGameInstance::~UTITVRGameInstance()
{
	TIDisconnect();
}*/


void UTITVRGameInstance::END()
{
	TIDisconnect();
	//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Black, FString(TEXT("STOP")));
}


void UTITVRGameInstance::START()
{
	TINormalModeCallback callback = 0;
	//a = TIConnectInt(3, TI_MODE_NORMAL, 0);

	//TIDisconnect();
	//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Black, FString(TEXT("START")));
}