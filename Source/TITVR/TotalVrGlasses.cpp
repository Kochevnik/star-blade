// Fill out your copyright notice in the Description page of Project Settings.

#include "TITVR.h"
#include "TotalVrGlasses.h"




UTotalVrGlasses::UTotalVrGlasses()
{
	Glases = LoadLibrary(TEXT("OculusPlugin190_2.dll"));
	(FARPROC &)OVR_GetSensorOrientation = GetProcAddress(Glases, "OVR_GetSensorOrientation");
	(FARPROC &)OVR_Initialize = GetProcAddress(Glases, "OVR_Initialize");
}

FQuat UTotalVrGlasses::GetOrientation()
{
	float w, x, y, z;
	bool b = OVR_GetSensorOrientation(0, &w, &x, &y, &z);
	//FQuat q = FQuat(z, -x, -y, w);		   
	return FQuat(z, -x, -y, w);
}

void UTotalVrGlasses::Init()
{
	if (Glases)
	{
		OVR_Initialize();
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString(TEXT("DLL_O OK & INIT OK")));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString(TEXT("DLL_O FAILED")));
	}
}

