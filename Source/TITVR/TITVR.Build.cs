// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class TITVR : ModuleRules
{
	public TITVR(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        DynamicallyLoadedModuleNames.AddRange(
           new string[]
           {
               // ... add any modules that your module loads dynamically here ...
           }
           );


        AddEngineThirdPartyPrivateStaticDependencies(Target
            // ... add any third party modules that your module depends on here ...
            );

        //string NatNetPath = Path.GetFullPath(Path.Combine(ModuleDirectory, "..", "..", "ThirdParty"));
        //string NatNetLibBinPath = "";


        /*if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            NatNetLibBinPath = Path.Combine(NatNetPath, "lib", "x64");
        }
        else if (Target.Platform == UnrealTargetPlatform.Win32)
        {
            NatNetLibBinPath = Path.Combine(NatNetPath, "lib");
        }*/

       // PublicLibraryPaths.Add(NatNetPath);
       // PublicAdditionalLibraries.Add(NatNetPath + "\\" + "OculusPlugin.dll");
        //PublicDelayLoadDLLs.Add("OculusPlugin.dll");
       // RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(NatNetPath, "OculusPlugin.dll")));
       // PublicDelayLoadDLLs.Add("Platform.dll");
        //RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(NatNetPath, "Platform.dll")));

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");
        // if ((Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Win64))
        // {
        //		if (UEBuildConfiguration.bCompileSteamOSS == true)
        //		{
        //			DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
        //		}
        // }
    }
}
