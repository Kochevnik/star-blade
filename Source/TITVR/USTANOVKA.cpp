// Fill out your copyright notice in the Description page of Project Settings.

#include "TITVR.h"
#include "TITVRGameInstance.h"
#include "USTANOVKA.h"


// Sets default values
AUSTANOVKA::AUSTANOVKA()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Platform = LoadLibrary(TEXT("TotalSBPlatform.dll"));
	//TITPlatform
	(FARPROC &)TIConnect = GetProcAddress(Platform, "TIConnect");

	(FARPROC &)TIConnectInt = GetProcAddress(Platform, "TIConnectInt");

	(FARPROC &)TISendF = GetProcAddress(Platform, "TISendF");

	(FARPROC &)TISendI = GetProcAddress(Platform, "TISendI");

	(FARPROC &)TIDisconnect = GetProcAddress(Platform, "TIDisconnect");

	(FARPROC &)TIGetLastError = GetProcAddress(Platform, "TIGetLastError");

	(FARPROC &)TIRead = GetProcAddress(Platform, "TIRead");
}

// Called when the game starts or when spawned
void AUSTANOVKA::BeginPlay()
{
	Super::BeginPlay();
	if (Platform)
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Blue, FString(TEXT("Platform Ok")));
	//GM = Cast<UTITVRGameInstance>(GetGameInstance());
	TIDisconnect();
	TINormalModeCallback callback = 0;
	TIConnectInt(3, 0, 0);
}

// Called every frame
void AUSTANOVKA::Tick( float DeltaTime )
{
	//Super::Tick( DeltaTime );
	//GM->TEST();
	
	ReadPlatformData();
	
	UpdatePlatformPosition();
	
	Log(DeltaTime);

	CalculateRotSpeed();

	UpdateGamePlatformPosition();

	AddMovement();
	//SetActorRotation(FRotator(GM->axis3, GM->axis4, 0));
}

// Called to bind functionality to input
void AUSTANOVKA::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AUSTANOVKA::UpdatePlatformPosition()
{
	//GM->TEST();

	/*short JP;
	short JY;
	short PP;
	short PY;

	char a, b;
	short c;*/

	if (TISendF(TargetPlatformPitch, TargetPlatformYaw, RotSpeed, 0))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("SEND")));
	/*if (GM->TIRead(&PY, &PP, &JY, &JP, &a, &b, &c))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));

	//GM->TIRead(&PY, &PP, &JY, &JP, &a, &b, &c);
	JoystickPitch = JP / 32000.f * 180;
	JoystickYaw = JY / 32000.f * 180;

	PlatformPitch = PP / 32000.f;
	PlatformYaw = PY / 32000.f;*/
}

void AUSTANOVKA::ReadPlatformPosition()
{
	//short JP;
	//short JY;
	short PP;
	short PY;

	
	if (TIRead(&PY, &PP, 0, 0, 0, 0, 0))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));

	PlatformPitch = PP / 32000.f;
	PlatformYaw = PY / 32000.f;

}

void AUSTANOVKA::CalculateRotSpeed()
{
	float time = UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
	
	/*RotSpeedPitch = JoystickPitch * time;
	RotSpeedYaw = JoystickYaw * time;

	TargetPlatformPitch += RotSpeedPitch;
	TargetPlatformYaw += RotSpeedYaw;*/

	if (FMath::Abs(JoystickPitch) > 0.04)
	{
		TargetPlatformPitch = (JoystickPitch > 0) ? (1) : (-1);
	}
	else
	{
		TargetPlatformPitch = 0;
	}

	if (FMath::Abs(JoystickYaw) > 0.04)
	{
		TargetPlatformYaw = (JoystickYaw > 0) ? (1) : (-1);
	}
	else
	{
		TargetPlatformYaw = 0;
	}

	RotSpeed = (FMath::Abs(JoystickPitch) + FMath::Abs(JoystickYaw)) / 2 * 0.99;
}

void AUSTANOVKA::UpdateGamePlatformPosition()
{
	FRotator rot;

	rot.Pitch = PlatformPitch;
	rot.Yaw = PlatformYaw;
	rot.Roll = 0.f;

	SetActorRotation(rot);
}

void AUSTANOVKA::JoystickInput()
{
	short JP;
	short JY;
	//short PP;
	//short PY;

	char Button;
	//short c;

	
	if (TIRead(0, 0, &JY, &JP, &Button, 0, 0))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));

	JoystickPitch = JP / 32000.f * 180;
	JoystickYaw = JY / 32000.f * 180;


}

void AUSTANOVKA::ReadPlatformData()
{
	short JP;
	short JY;
	short PP;
	short PY;

	char Button, b;
	short c;

	
	if (TIRead(&PY, &PP, &JY, &JP, &Button, &b, &c))
	GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));

	//JoystickPitch = JP;// / 32000.f * 180;
	//JoystickYaw = JY;// / 32000.f * 180;

	//JoystickPitch = (JP < 0) ? ((JP + 64) * 180 / 32704.f) : (JP * 180 / 32704.f); //���������� � -180 180
	//JoystickYaw = (JY < 0) ? ((JY + 64) * 180 / 32704.f) : (JY * 180 / 32704.f);

	JoystickPitch = (JP < 0) ? ((JP + 64) / 32704.f) : (JP / 32704.f); //���������� � -1 1
	JoystickYaw = (JY < 0) ? ((JY + 64) / 32704.f) : (JY / 32704.f);

	PlatformPitch = (PP < 0) ? ((PP + 1) / 32768.f) : (PP / 32768.f);
	PlatformYaw = (PY < 0) ? ((PY + 0) / 32767.f) : (PY / 32767.f);

	PlatformPitch = (PlatformPitch > 0) ? (PlatformPitch * 89) : (PlatformPitch * 42);
	PlatformYaw *= 180 * 300;

	

	if ((Button & (char)1) == 1)
	{
		LButton = true;
	}
	else
	{
		LButton = false;
	}

	if ((Button & (char)2) == 2)
	{
		RButton = true;
	}
	else
	{
		RButton = false;
	}
	
	//char * cButton = &Button;
	GEngine->AddOnScreenDebugMessage(-1, 0.0002, FColor::Green, FString::SanitizeFloat((int8)Button));
}


void AUSTANOVKA::AddMovement()
{
	if (LButton)
		AddActorLocalOffset(FVector(5,0,0));
}

void AUSTANOVKA::Log(float DeltaTime)
{
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Green, FString(GM->TIGetLastError()));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Yellow, FString(TEXT("Error:  ")) + FString::SanitizeFloat(GM->errorP));
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("PlatformPitch:  ")) + FString::SanitizeFloat(PlatformPitch));
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("PlatformYaw:  ")) + FString::SanitizeFloat(PlatformYaw));
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("JoystickPitch:  ")) + FString::SanitizeFloat(JoystickPitch));
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("JoystickYaw:  ")) + FString::SanitizeFloat(JoystickYaw));

	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Magenta, FString(TEXT("RotSpeed:  ")) + FString::SanitizeFloat(RotSpeed));

	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("TargetPlatformPitch:  ")) + FString::SanitizeFloat(TargetPlatformPitch));
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("TargetPlatformYaw:  ")) + FString::SanitizeFloat(TargetPlatformYaw));

	if (LButton)
	{
		GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Silver, FString(TEXT("LButton")));
	}
	if (RButton)
	{
		GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Silver, FString(TEXT("RButton")));
	}
}