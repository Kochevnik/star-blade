// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "TITVRGameInstance.generated.h"

/**
 * 
 */
//#define TI_CALL __stdcall
typedef void (__stdcall *TINormalModeCallback)(int reason);


UCLASS()
class TITVR_API UTITVRGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:

	UTITVRGameInstance(const FObjectInitializer& ObjectInitializer);

	void virtual Init() override;

	void virtual StartGameInstance() override;
	
	short axis1;

	short axis2;

	short axis3;

	short axis4;

	int errorP;

	FQuat GetOrientation(int id);

	HMODULE Oculus;

	HMODULE Platform;

	int error = 0;
	
	int a = -1;
	//Oculus
	bool(*OVR_GetSensorOrientation)(int id, float* w, float* x, float* y, float* z);

	bool(*OVR_Initialize)();

	//Platform
	int (*TIConnect)(const char *portName, int mode, TINormalModeCallback callback);

	int (*TIConnectInt)(int portName, int mode, TINormalModeCallback callback);

	int (*TISendF)(float axis1, float axis2, float speed, unsigned int effects);

	int (*TISendI)(short axis1, short axis2, unsigned int speed, unsigned int effects);

	int (*TIDisconnect)();

	char* (*TIGetLastError)();

	int (*TIRead)(short * axis1, short * axis2, short * axis3, short * axis4, char * buttons, char * effects, short * speed);

	//void GetPlatformOrientation(int16 * PlatformPitch, int16 * PlatformYaw);

	//void GetJoiystickOrientation(int16 * JPitch, int16 * JYaw);

	void TEST();

	UFUNCTION(BlueprintCallable, Category = "PLATFORMA")
	void END();

	UFUNCTION(BlueprintCallable, Category = "PLATFORMA")
	void START();
};


//enum
//{

	/*
	*   � ���������� ������ ��������� ���������� ����� ���������� ������.
	*   ��� ���� ��������� �������������� ��������� �������� ��������� ���������.
	*   ���� ����� �������� �������� ������� � ������������� � ������������� �
	*   ���������� (�� ����������� ������ �������).
	*/
	//TI_MODE_NORMAL,

	// -----------------------------------------------------------------------------

	/*
	*   � ���������������� ������ ����� �� ��������� � ��� ������ ��������
	*   ���������� �� ���������. ��������� ��������� ������ ��������������
	*   ��������� ��� ���������� ���������� ����������. �������������
	*   ��������� ��������� � �������� �� ���� 20 ��� � �������.
	*/
	//TI_MODE_IMMEDIATE

//};