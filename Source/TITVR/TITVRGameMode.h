// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "TITVRGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TITVR_API ATITVRGameMode : public AGameMode
{
	GENERATED_BODY()
public:

	ATITVRGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float Time) override;

	FQuat GetOrientation(int id);
	
	//HMODULE hLib;
	//int error = 0;
	//LPCWSTR name = (WCHAR*)("OculusPlugin.dll");
	//bool(*OVR_GetSensorOrientation)(int id, float* w, float* x, float* y, float* z);
	//bool (*OVR_Initialize)();
};
