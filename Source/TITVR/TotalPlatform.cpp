// Fill out your copyright notice in the Description page of Project Settings.

#include "TITVR.h"
#include "TotalPlatform.h"



UTotalPlatform::UTotalPlatform()
{

	Platformdll = LoadLibrary(TEXT("TotalSBPlatform.dll"));
	
	(FARPROC &)TIConnect = GetProcAddress(Platformdll, "TIConnect");

	(FARPROC &)TIConnectInt = GetProcAddress(Platformdll, "TIConnectInt");

	(FARPROC &)TISendF = GetProcAddress(Platformdll, "TISendF");

	(FARPROC &)TISendI = GetProcAddress(Platformdll, "TISendI");

	(FARPROC &)TIDisconnect = GetProcAddress(Platformdll, "TIDisconnect");

	(FARPROC &)TIGetLastError = GetProcAddress(Platformdll, "TIGetLastError");

	(FARPROC &)TIRead = GetProcAddress(Platformdll, "TIRead");


	TIDisconnect();
	TINormalModeCallback callback = 0;
	TIConnectInt(3, TI_MODE_NORMAL, 0);

}

void UTotalPlatform::UpdatePlatformData()
{
	short JP;
	short JY;
	short PP;
	short PY;

	char Button, b;
	short c;
	//�������� ������ �� ���������
	if (TISendF(TargetPlatformPitch, TargetPlatformYaw, RotSpeed, 0))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("SEND")));
	//������ ������ � ���������
	if (TIRead(&PY, &PP, &JY, &JP, &Button, &b, &c))
		GEngine->AddOnScreenDebugMessage(-1, 0.000001, FColor::Green, FString(TEXT("READ")));

	//JoystickPitch = JP;// / 32000.f * 180;
	//JoystickYaw = JY;// / 32000.f * 180;

	//JoystickPitch = (JP < 0) ? ((JP + 64) * 180 / 32704.f) : (JP * 180 / 32704.f); //���������� � -180 180
	//JoystickYaw = (JY < 0) ? ((JY + 64) * 180 / 32704.f) : (JY * 180 / 32704.f);

	JoystickPitch = (JP < 0) ? ((JP + 64) / 32704.f) : (JP / 32704.f); //���������� � -1 1
	JoystickYaw = (JY < 0) ? ((JY + 64) / 32704.f) : (JY / 32704.f);

	PlatformPitch = (PP < 0) ? ((PP + 1) / 32768.f) : (PP / 32768.f);
	PlatformYaw = (PY < 0) ? ((PY + 0) / 32767.f) : (PY / 32767.f);

	PlatformPitch = (PlatformPitch > 0) ? (PlatformPitch * 89) : (PlatformPitch * 42);
	PlatformYaw *= 180 * 300; //��� ��� ���� ����� ���������


	//������

	LButton = ((Button & (char)1) == 1) ? (true) : (false);
	RButton = ((Button & (char)2) == 2) ? (true) : (false);
	LButtonFire = ((Button & (char)4) == 4) ? (true) : (false);
	RButtonFire = ((Button & (char)8) == 8) ? (true) : (false);


	/*if ((Button & (char)1) == 1)
	{
		LButton = true;
	}
	else
	{
		LButton = false;
	}

	if ((Button & (char)2) == 2)
	{
		RButton = true;
	}
	else
	{
		RButton = false;
	}*/

	//char * cButton = &Button;
	GEngine->AddOnScreenDebugMessage(-1, 0.0002, FColor::Green, FString::SanitizeFloat((int8)Button));


}

FRotator UTotalPlatform::GetGamePlatformRot()
{
	return FRotator(GamePlatformPitch, GamePlatformYaw, 0);
}

void UTotalPlatform::SetTargetPlatformPosition(float Pitch, float Yaw)
{
	TargetPlatformPitch = Pitch;
	TargetPlatformYaw = Yaw;
}

void UTotalPlatform::CalculateGameData()
{
	float time = UGameplayStatics::GetWorldDeltaSeconds(GetWorld());

	/*RotSpeedPitch = JoystickPitch * time;
	RotSpeedYaw = JoystickYaw * time;

	TargetPlatformPitch += RotSpeedPitch;
	TargetPlatformYaw += RotSpeedYaw;*/

	if (FMath::Abs(JoystickPitch) > 0.2)//��� ���� ���������!!
	{
		TargetPlatformPitch = (JoystickPitch > 0) ? (1) : (-1); //��� ���� ��������� !!
	}
	else 
	{
		TargetPlatformPitch = 0;
	}

	if (FMath::Abs(JoystickYaw) > 0.2) //��� ���� ���������!!
	{
		TargetPlatformYaw = (JoystickYaw > 0) ? (0.2) : (-0.2); //��� ���� ���������!!
	}
	else
	{
		TargetPlatformYaw = 0;
	}

	RotSpeed = 0.2; //��� ���� ���������!!
}
