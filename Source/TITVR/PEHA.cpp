// Fill out your copyright notice in the Description page of Project Settings.

#include "TITVR.h"
#include "TITVRGameInstance.h"
#include "PEHA.h"




// Sets default values
APEHA::APEHA()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Glass = CreateDefaultSubobject<USceneComponent>(TEXT("Glas"));
	
	
	
	Oculus = LoadLibrary(TEXT("OculusPlugin190_2.dll"));
	
	//void * dll = FPlatformProcess::GetDllHandle()

	if (GetProcAddress(Oculus, "OVR_GetSensorOrientation"))
	{
	int	error = 1;
	}

	//OculusPlugin
	(FARPROC &)OVR_GetSensorOrientation = GetProcAddress(Oculus, "OVR_GetSensorOrientation");
	(FARPROC &)OVR_Initialize = GetProcAddress(Oculus, "OVR_Initialize");
}

// Called when the game starts or when spawned
void APEHA::BeginPlay()
{
	Super::BeginPlay();
	//GM = Cast<UTITVRGameInstance>(GetGameInstance());
	
	if (Oculus)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString(TEXT("DLL_O OK")));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString(TEXT("DLL_O FAILED")));
	}


	if (GetProcAddress(Oculus, "OVR_GetSensorOrientation"))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString(TEXT("FUNC OK")));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString(TEXT("FUNC FAILED")));
	}

	if (OVR_Initialize())
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString(TEXT("INIT OK")));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString(TEXT("INIT FAILED")));
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Yellow, FString(TEXT("PLATFORM:  ")) + FString::SanitizeFloat(GM->a));
	
}

// Called every frame
void APEHA::Tick( float DeltaTime )
{
	//Super::Tick( DeltaTime );
	//FRotator drot = FRotator(GM->GetOrientation(0));

	//SetActorRotation(FRotator(drot.Yaw, drot.Pitch, drot.Roll));
	FQuat q = GetOrientation(0);
	q.Normalize();
	SetActorRotation(q);
	//Glass->SetWorldRotation(q);

	//GM->TEST();

	//SetActorRotation(FRotator(GM->axis3, GM->axis4,0));

	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Green, FString(GM->TIGetLastError()));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Yellow, FString(TEXT("Error:  ")) + FString::SanitizeFloat(GM->errorP));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("X:  ")) + FString::SanitizeFloat(q.X));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("Y:  ")) + FString::SanitizeFloat(q.Y));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("Z:  ")) + FString::SanitizeFloat(q.Z));
	//GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Blue, FString(TEXT("A4:  ")) + FString::SanitizeFloat(GM->axis4));*/
	//UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetController()->SetActorRotation(q);
	//UGameplayStatics::GetPlayerPawn(GetWorld(), 1)->GetController()->SetControlRotation(q);
}

// Called to bind functionality to input
void APEHA::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

FQuat APEHA::GetOrientation(int id)
{
	float w, x, y, z;
	bool b = OVR_GetSensorOrientation(id, &w, &x, &y, &z);	   
	return FQuat(z, -x, -y, w);

}