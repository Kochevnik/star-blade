// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "TotalVrGlasses.generated.h"

/**
 * 
 */
UCLASS()
class TITVR_API UTotalVrGlasses : public UObject
{
	GENERATED_BODY()

public:
	UTotalVrGlasses();

	FQuat GetOrientation();

	void Init();
	
	bool(*OVR_GetSensorOrientation)(int id, float* w, float* x, float* y, float* z);

	bool(*OVR_Initialize)();

	HMODULE Glases;
};
