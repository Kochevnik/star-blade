// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "TotalPlatform.generated.h"

/**
 * 
 */

typedef void(__stdcall *TINormalModeCallback)(int reason);

UCLASS()
class TITVR_API UTotalPlatform : public UObject
{
	GENERATED_BODY()
	
public:

	UTotalPlatform();

	HMODULE Platformdll;

	//Dlls functions

	int(*TIConnect)(const char *portName, int mode, TINormalModeCallback callback);

	int(*TIConnectInt)(int portName, int mode, TINormalModeCallback callback);

	int(*TISendF)(float axis1, float axis2, float speed, unsigned int effects);

	int(*TISendI)(short axis1, short axis2, unsigned int speed, unsigned int effects);

	int(*TIDisconnect)();

	char* (*TIGetLastError)();

	int(*TIRead)(short * axis1, short * axis2, short * axis3, short * axis4, char * buttons, char * effects, short * speed);
	
	//PlatformData
	//Buttons
	bool LButton;

	bool RButton;

	bool LButtonFire;

	bool RButtonFire;

	float RotSpeed; //�������� ��������

	float PlatformPitch; //������� ��������� ��������� �� ��� �������

	float PlatformYaw; //������� ��������� ��������� �� ��� ��������

	float TargetPlatformPitch = 0; //�������� ������� ��������� ��������� �� ��� ������� (�������� �������� �� ������ ���)

	float TargetPlatformYaw = 0; //�������� ������� ��������� ��������� �� ��� ��������

	float JoystickPitch; //������� ��������� ��������� �� ��� �������

	float JoystickYaw; //������� ��������� ��������� �� ��� ��������

	float GamePlatformPitch = 0; //�������� ��������� ������ � ���� �� ��� �������

	float GamePlatformYaw = 0; //�������� ��������� ������ � ���� �� ��� ��������

	float RotSpeedPitch; //??

	float RotSpeedYaw; //??

	void UpdatePlatformData(); //���������� ����������� (�������� ����� �� ��������� � ������)

	FRotator GetGamePlatformRot(); //���������� ��������� ����� � ���� ��������� �� ��������� ���������

	void SetTargetPlatformPosition(float Pitch, float Yaw); //��������� �������� ��������� ���������

	void CalculateGameData();



};


enum
{

	/*
	*   � ���������� ������ ��������� ���������� ����� ���������� ������.
	*   ��� ���� ��������� �������������� ��������� �������� ��������� ���������.
	*   ���� ����� �������� �������� ������� � ������������� � ������������� �
	*   ���������� (�� ����������� ������ �������).
	*/
	TI_MODE_NORMAL,

	// -----------------------------------------------------------------------------

	/*
	*   � ���������������� ������ ����� �� ��������� � ��� ������ ��������
	*   ���������� �� ���������. ��������� ��������� ������ ��������������
	*   ��������� ��� ���������� ���������� ����������. �������������
	*   ��������� ��������� � �������� �� ���� 20 ��� � �������.
	*/
	TI_MODE_IMMEDIATE

};