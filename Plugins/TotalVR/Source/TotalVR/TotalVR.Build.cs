// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

using Path = System.IO.Path;
using UnrealBuildTool;

public class TotalVR : ModuleRules
{
	public TotalVR(TargetInfo Target)
	{
		
		PublicIncludePaths.AddRange(
			new string[] {
				"TotalVR/Public"
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"TotalVR/Private",
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        AddEngineThirdPartyPrivateStaticDependencies(Target
            // ... add any third party modules that your module depends on here ...
            );

        /*string Dllpath = Path.GetFullPath(Path.Combine(ModuleDirectory, "..", "..", "ThirdParty"));
        //string NatNetLibBinPath = "";


        PublicLibraryPaths.Add(Dllpath);
        PublicDelayLoadDLLs.Add("OculusPlugin190_2.dll");
        RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(Dllpath, "OculusPlugin190_2.dll")));
        PublicAdditionalLibraries.Add(Path.Combine(Dllpath, "OculusPlugin190_2.dll"));*/
    }
}
