// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "USTANOVKA.generated.h"

UCLASS()
class TITVR_API AUSTANOVKA : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AUSTANOVKA();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	bool LButton;

	bool RButton;

	float RotSpeed;

	float PlatformPitch;

	float PlatformYaw;

	float TargetPlatformPitch;

	float TargetPlatformYaw;

	float JoystickPitch;

	float JoystickYaw;

	float RotSpeedPitch;

	float RotSpeedYaw;

	//class UTITVRGameInstance * GM;

	void UpdatePlatformPosition();

	void ReadPlatformPosition();
	
	void CalculateRotSpeed();

	void JoystickInput();
	
	void ReadPlatformData();

	void Log(float DeltaTime);

	void UpdateGamePlatformPosition();

	void AddMovement();

	//Platform
	int(*TIConnect)(const char *portName, int mode, TINormalModeCallback callback);

	int(*TIConnectInt)(int portName, int mode, TINormalModeCallback callback);

	int(*TISendF)(float axis1, float axis2, float speed, unsigned int effects);

	int(*TISendI)(short axis1, short axis2, unsigned int speed, unsigned int effects);

	int(*TIDisconnect)();

	char* (*TIGetLastError)();

	int(*TIRead)(short * axis1, short * axis2, short * axis3, short * axis4, char * buttons, char * effects, short * speed);

	//void GetPlatformOrientation(int16 * PlatformPitch, int16 * PlatformYaw);

	//void GetJoiystickOrientation(int16 * JPitch, int16 * JYaw);

	

	/*UFUNCTION(BlueprintCallable, Category = "PLATFORMA")
		void END();

	UFUNCTION(BlueprintCallable, Category = "PLATFORMA")
		void START();*/

	HMODULE Platform;

	int error = 0;
};
