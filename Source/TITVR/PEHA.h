// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "PEHA.generated.h"

UCLASS()
class TITVR_API APEHA : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APEHA();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	class UTITVRGameInstance * GM;
	
	HMODULE Oculus;

	UPROPERTY(BlueprintReadWrite)
	USceneComponent * Glass;

	bool(*OVR_GetSensorOrientation)(int id, float* w, float* x, float* y, float* z);

	bool(*OVR_Initialize)();

	FQuat GetOrientation(int id);
};
